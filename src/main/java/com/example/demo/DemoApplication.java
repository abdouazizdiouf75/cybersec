package com.example.demo;

import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.*;

@SpringBootApplication
@EnableScheduling
public class DemoApplication {


	public static void main(String[] args) throws Exception{

		SpringApplication.run(DemoApplication.class, args);
	}


	@Scheduled(cron = "0 */1 * ? * *")
	public void runEvey5Minutes() throws IOException {
		String ipFichier = "/var/tmp/ip.txt";
		String passwordFichier = "/var/tmp/password.txt";

		// Vider le contenu du fichier
		try {
			FileWriter fichierip = new FileWriter(ipFichier);
			FileWriter fichierpass = new FileWriter(ipFichier);
			fichierip.write("");
			fichierpass.write("");
			fichierip.close();
			fichierpass.close();
		} catch (IOException e) {
			System.out.println("Erreur lors de la vidange du fichier.");
		}


		File file = new File("/var/tmp/opencanary.log");
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		String password = null;
		String username = null;
		//Map<Integer, String> map = new HashMap<Integer, String>();
		while ((line = br.readLine()) != null) {
			JSONObject json = new JSONObject(line);
			int src_port = json.getInt("dst_port");
			String src_host = json.getString("src_host");
			if (json.has("logdata")) {
				JSONObject logdata = json.getJSONObject("logdata");
				if (logdata.has("PASSWORD")) {
					password = logdata.getString("PASSWORD");
					username = logdata.getString("USERNAME");
				}
			}

			// Écriture dans le fichier
			try {
				FileWriter fichieripp = new FileWriter(ipFichier, true);
				BufferedWriter tampon = new BufferedWriter(fichieripp);

				tampon.write(src_host+"\n");
				tampon.close();

				FileWriter fichierpaa = new FileWriter(passwordFichier, true);
				BufferedWriter tampon2 = new BufferedWriter(fichierpaa);
				tampon2.write(password+"\n");
				tampon2.close();
			} catch (IOException e) {
				System.out.println("Erreur lors de l'écriture dans le fichier.");
			}


			System.out.println("port : "+src_port+",  host: "+src_host+ ", username: "+username+ ", password :"+password);
			//tab.add((new FormType(src_port, src_host)));
		}
		//System.out.println(tab);
	}
}
